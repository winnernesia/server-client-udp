﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace client_udp
{
    class Program
    {
        static void Main(string[] args)
        {
            UdpClient udpclient = new UdpClient("10.252.39.118", 9050);
            IPEndPoint ep = null;

            Console.WriteLine(" [Client Started]\n [Change Char To The Next 4]\n");

            Console.Write(" Ketik Kalimat/huruf      : ");
            string pesan = Console.ReadLine();
            byte[] dataubah = Encoding.ASCII.GetBytes(pesan);
            udpclient.Send(dataubah, dataubah.Length, ep);

            char[] dstakonvert = pesan.ToCharArray();
            Console.Write(" Terkonvert    : ");
            for (int i = 0; i < dstakonvert.Length; i++)
            {
                byte[] recv = udpclient.Receive(ref ep);
                string datsterima = Encoding.ASCII.GetString(recv);
                Console.Write(datsterima);
            }

            Console.WriteLine("\n");

            while (true)
            {
                Console.Write(" Ketik Kalimat/huruf      : ");
                string client_msg = Console.ReadLine();
                byte[] ncdata = Encoding.ASCII.GetBytes(client_msg);
                udpclient.Send(ncdata, ncdata.Length);

                char[] client_char = client_msg.ToCharArray();
                Console.Write(" Terkonvert    : ");
                for (int i = 0; i < client_char.Length; i++)
                {
                    byte[] recv = udpclient.Receive(ref ep);
                    string datsterima = Encoding.ASCII.GetString(recv);
                    Console.Write(datsterima);
                }

                Console.WriteLine("\n===========================\n");
            }
        }
    }
}