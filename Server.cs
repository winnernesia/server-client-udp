﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace server_udp
{
    class server
    {
        static void Main(string[] uyabbb)
        {
            int tambah = 4;

            UdpClient udpclient = new UdpClient(9050);
            Console.WriteLine(" server memulai\n\n");
            IPEndPoint ep = null;

            byte[] ncdata1 = udpclient.Receive(ref ep);
            string pesan = Encoding.ASCII.GetString(ncdata1);
            Console.WriteLine(" Huruf/kata awal      : " + pesan);

            char[] datsterima = pesan.ToCharArray();
            Console.Write(" Terkonvert    : ");
            for (int i = 0; i < datsterima.Length; i++)
            {
                int hurufff = datsterima[i];

                if (hurufff != 32)
                {
                    hurufff += tambah;
                }

                if (hurufff > 122)
                {
                    hurufff -= 26;
                }

                datsterima[i] = (char)(hurufff);
                Console.Write(datsterima[i]);

                string konverted = Convert.ToString(datsterima[i]);
                byte[] sent = Encoding.ASCII.GetBytes(konverted);
                udpclient.Send(sent, sent.Length, ep);
            }

            Console.WriteLine("\n");

            while (true)
            {
                byte[] ncdata = udpclient.Receive(ref ep);
                string pesanan = Encoding.ASCII.GetString(ncdata);
                Console.WriteLine(" Huruf/kata awal      : " + pesanan);
                
                char[] client_char = pesanan.ToCharArray();
                Console.Write(" Terkonvert    : ");
                for (int i = 0; i < client_char.Length; i++)
                {
                    int hurufff = client_char[i];

                    if (hurufff != 32)
                    {
                        hurufff += tambah;
                    }

                    if (hurufff > 122)
                    {
                        hurufff -= 26;
                    }

                    client_char[i] = (char)(hurufff);
                    Console.Write(client_char[i]);

                    string konverted = Convert.ToString(client_char[i]);
                    byte[] sent = Encoding.ASCII.GetBytes(konverted);
                    udpclient.Send(sent, sent.Length, ep);
                }

                Console.WriteLine("\n");
            }

            udpclient.Close();
        }
    }
}